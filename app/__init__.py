from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.bootstrap import Bootstrap
from flask.ext.mail import Mail
from flask.ext.moment import Moment

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

bootstrap = Bootstrap()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'login'

login_manager.init_app(app)
bootstrap.init_app(app)

mail = Mail(app)
moment = Moment(app)

from app import views, models