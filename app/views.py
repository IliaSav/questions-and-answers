from flask import render_template, flash, redirect, url_for, request
from flask.ext.login import login_required, login_user, logout_user, current_user
from .forms import LoginForm, RegistrationForm, QuestionForm, AnswerForm
from .models import User, Question, Answer, Votes
from . import login_manager
from app import app, db, mail
from flask.ext.mail import Message


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


def send_email(to, subject, template, **kwargs):
    msg = Message(app.config['MAIL_SUBJECT_PREFIX'] + subject,
                  sender=app.config['MAIL_SENDER'], recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    mail.send(msg)


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    form = QuestionForm()
    if current_user and form.validate_on_submit():
        question = Question(body=form.body.data, user_id=current_user._get_current_object().id)
        db.session.add(question)
        db.session.commit()
        return redirect(url_for('.index'))
    questions = Question.query.order_by(Question.timestamp.desc()).all()
    return render_template("index.html",
                           title='Home',
                           questions=questions,
                           form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('index'))
        flash('Invalid username or password.')
    return render_template('login.html', form=form)


@app.route('/logout')
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(nickname=form.username.data,
                    email= form.email.data,
                    password=form.password.data)
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        send_email(user.email, 'Confirm Your Account', 'email', user=user, token=token)
        flash('A confirmation email has been sent to you by email.')
        return redirect(url_for('index'))
    return render_template('register.html', form=form)


@app.route('/confirm/<token>')
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect(url_for('index'))
    if current_user.confirm(token):
        flash('You have confirmed your account. Thanks!')
    else:
        flash('The confirmation link is invalid or has expired.')
    return redirect(url_for('index'))


@app.before_first_request
def before_request():
    if current_user.is_authenticated() and not current_user.confirmed:
        return redirect(url_for('unconfirmed'))


@app.route('/unconfirmed')
def unconfirmed():
    if current_user.is_anonymous() or current_user.confirmed():
        return redirect('index')
    return redirect('confirm')


@app.route('/confirm')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    send_email('email', 'Confirm Your Account', user, token=token)
    flash('A new confirmation email has been sent to you by email.')
    return redirect(url_for('index'))


@app.route('/user/<nickname>')
def user(nickname):
    user = User.query.filter_by(nickname=nickname).first()
    if user is None:
        flash('User %s not found.' % nickname)
        return redirect(url_for('index'))
    questions = user.questions.order_by(Question.timestamp.desc()).all()
    return render_template('user.html',
                           user=user,
                           questions=questions)


@app.route('/vote/<user_id>/<answer_id>')
@login_required
def vote(user_id, answer_id):
    if current_user.confirmed:
        user_vote = Votes.query.filter_by(answer_id=answer_id).filter_by(user_id=user_id).first()
        if user_vote:
            db.session.delete(user_vote)
            db.session.commit()
        else:
            user_vote = Votes(answer_id=answer_id, user_id=user_id)
            db.session.add(user_vote)
            db.session.commit()
        answer = Answer.query.filter_by(id=answer_id).first()

    return redirect(url_for('.question', id=answer.question_id))


@app.route('/question/<int:id>', methods=['GET', 'POST'])
def question(id):
    question = Question.query.get_or_404(id)
    form = AnswerForm()
    if form.validate_on_submit() and current_user.confirmed:
        answer = Answer(body=form.body.data, question=question, user=current_user._get_current_object())
        db.session.add(answer)
        db.session.commit()
        flash('Your comment has been published.')
        return redirect(url_for('.question', id=question.id))
    answers = question.answers.order_by(Answer.timestamp.asc())
    votes = Votes.query.all()
    return render_template('question.html',form=form, question=question, answers=answers, votes=votes)